Diba Security Policy
=======================

https://www.drupal.org/project/diba_security_policy

What Is This?
-------------

This module overwrites some Drupal functionalities to apply diba security policy.

* Remove head meta tag generator to reduce fingerprint software information.
* Adds XSS protection header in block mode.
* Adds Strict-Transport-Security header to tell browsers that the site it should only be accessed using HTTPS.
* Prevents user enumeration using password request form. Drupal core shows different messages when you input a valid or invalid email o username, this module equals messages in both situations to avoid user enumeration.

How To Install
--------------

* If you use composer adds using: composer require 'drupal/diba_security_policy'
* If you not use composer download and adds the module in contrib folder.
* Enable the module in Admin menu > Site building > Modules.
